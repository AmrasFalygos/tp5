package programmes;

import        gestionprojets.Salarie;
import static gestionprojets.Tris.trierParNomPrenom;
import static gestionprojets.Entreprise.getTousLesSalaries;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple01 {

    public  void executer() {
      
        
        System.out.printf("\nListe de salariés à la date du: %-8s \n\n", aujourdhuiChaine());
      
        trierParNomPrenom(getTousLesSalaries());
        
        
        for(Salarie sal: getTousLesSalaries()){
        
              System.out.printf(" %3d %-15s\n", sal.getId(),sal.getNom());
        
        }
        
        System.out.println();
    }
}

