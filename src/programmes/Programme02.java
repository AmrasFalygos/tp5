package programmes;

import static gestionprojets.Entreprise.getTousLesPoles;
import        gestionprojets.Salarie;
import static gestionprojets.Tris.trierParNomPrenom;
import static utilitaires.UtilDate.aujourdhuiChaine;
import        gestionprojets.Pole;

public class Programme02 {

    public  void executer() {
      
       System.out.printf("\n Liste de salariés à la date du: %-8s \n\n", aujourdhuiChaine());
        
       for(Pole pole : getTousLesPoles()){
       
           System.out.printf(" Pôle %-4s: %-30s\n\n",pole.getCodePole(),pole.getNomPole()); 
            
           for(Salarie sal: trierParNomPrenom( pole.getLesSalaries()) ) {
              
              System.out.print(" ");
              sal.afficher();
              System.out.println();
           }
        
           
           System.out.printf( "\n  Moyenne des salaires  Hommes = %6.2f€ Femmes = %6.2f€ Global = %6.2f€\n",
              pole.moyenneSalaires("F"),pole.moyenneSalaires("M"), pole.moyenneSalaires()
           );
           
           System.out.printf( "  Effectifs             Hommes = %4d     Femmes = %4d     Global = %4d\n\n",
              pole.effectif("M"),pole.effectif("F") , pole.effectif()
           );
         
           
           
        }
        System.out.println();
    }
  
   
}

