package programmes;

import gestionprojets.Salarie;
import static gestionprojets.Entreprise.getTousLesSalaries;
import static utilitaires.UtilDate.aujourdhuiChaine;

public class Exemple03 {

    public  void executer() {
      
     
       System.out.printf("\nListe de salariés à la date du: %-8s \n\n", aujourdhuiChaine());
      
        
        for(Salarie sal: getTousLesSalaries()){
        
              sal.afficher();
              System.out.println();
        
        }
        
        System.out.println();
        
       
    }
}

